## RoboMasterOS的特点

### 1.ROS兼容

RoboMasterOS 是基于ROS的，__本质是一个ROS metaPackage (Stack)__，针对robomaster比赛，集成了一些开发的基本工具，并提供了一些开发功能包，以及开发样例，以达到加速开发的目的。

正因为基于ROS，所以可以获得ROS的相关优势：

* 可使用ROS自带调试工具，方便调试。
* 无缝对接其他ROS开源项目，加快项目开发。

正因为基于ROS，所以RoboMasterOS编程语言为：

- C++和Python（C++为主）

> ROS (Robot Operating System, 机器人操作系统) 提供一系列程序库和工具以帮助软件开发者创建机器人应用软件。它提供了硬件抽象、设备驱动、函数库、可视化工具、消息传递和软件包管理等诸多功能。（来自wiki.ros.org）
>
> * 总之，一句话，ROS提供了相应工具来开发机器人软件，不但如此，它还有一个庞大的社区，上面有各种基于ROS的开源项目，可以用来开发自己的ROS程序，RoboMasterOS也是其中的一个基于ROS的开源项目。
>
> 更多内容参考官网：[www.ros.org](http://www.ros.org)

### 2.扩展能力

RoboMasterOS采用了组件化设计，编写了若干核心组件，具有十分强大的扩展能力，方便进行快速的二次开发：

* 乐高特性：像搭建乐高一样，利用RMOS中的组件，构建自己的应用。
* 局部DIY特性：对于RMOS的一些组件，通过规范接口，可以自行定制编程实现，并进行替换，进行局部DIY设计，而不需要从头开始。

RoboMasterOS提供相关基础组件以及部分功能组件。基础组件可以进行复用，达到加快开发的目的，使开发者专注于算法开发，甚至可以利用这些组件搭建一个完整的机器人软件demo。

### 3.丰富的内容

RoboMasterOS不仅仅是一个Software Stack，也是一个Technology Stack。RoboMasterOS提供了开发RoboMaster相关开发技巧：

* RoboMaster相关软件栈，如相机，通信，自瞄等功能模块，可供选择，进行快速开发。
* RoboMaster算法技术文档，不仅仅是代码，包含相关技术文档，方便解读代码，以及进行自己的优化与调整。
* RoboMaster开发指南，包含RoboMasterOS使用文档，以及基于RoboMasterOS的完整机器人开发案例与开发流程，非常适合新手快速上手。

RoboMasterOS是一个开源软件栈与技术栈（Open Stack，OS），更致力于成为操作系统（Operating System，OS），为开发者提供像操作系统(OS)一样的便利的开发工具。

> RoboMasterOS提供了开发RoboMaster机器人案例，提供了完整的开发流程，包含着针对RoboMaster机器人的软件开发技巧，是一个完整的RoboMaster软件开发体系。所以，RoboMasterOS不是一个OS(Operating System),但是致力于成为一个“OS”(Operating System)。

## 设计特点

#### 1.基于ROS设计

* RMOS是一个ROS MetePackage，是一个基于ROS的项目。
* RMOS虽然基于ROS，但尽可能与ROS解耦合，未来考虑支持其他平台，或者自己开发平台。
* RMOS大部分ROS功能包，会有明显的ROS与算法分层结构，即算法部分单独成为一个模块，与ROS解耦。

#### 2.模块化设计

- 对于RMOS的大部分ROS功能包，即可以作为一个节点单独工作，也可以作为一个库被依赖，支持ROS二次开发。

#### 3.组件化设计

* 采用组件化设计提高复用
* 提高通用性

> 组件化与模块化
>
> ![](imgs/index_2.png) 
> 对于RMOS的大部分ROS功能包，采用组件化设计与模块化设计，可以方便的使用现成的模块，构建自己的应用，也可以将自己的模块与现有的模块进行组合来构建应用。

#### 4.接口设计

* 通过规范接口，可以使得模块可以被替换。

<br>

