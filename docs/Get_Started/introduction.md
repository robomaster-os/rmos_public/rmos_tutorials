

![](imgs/index.png ":no-zoom")

### 什么是RoboMasterOS ?

**RoboMasterOS（RoboMaster Open Stack，简称，RMOS）**是为RoboMaster高性能计算平台软件开发提供快速开发工具的一个通用统一的`开源软件栈`。

> [RoboMaster竞赛][2]，全称为`全国大学生机器人大赛RoboMaster机甲大师赛` 。
>
> * 全国大学生机器人[RoboMaster][2]大赛，是一个涉及“机器视觉”、“嵌入式系统设计”、“机械控制”、“人机交互”等众多机器人相关技术学科的机器人比赛。
> * 在RoboMaster 2019赛季中，参赛队伍需自主研发不同种类和功能的机器人，在指定的比赛场地内进行战术对抗，通过操控机器人发射弹丸攻击敌方机器人和基地。每局比赛7分钟，比赛结束时，基地剩余血量高的一方获得比赛胜利。
>
> 更多详情参考官网：[www.robomaster.com][2]

其中，在RoboMaster比赛中，“机器视觉”，“自主导航”等相关内容涉及到高性能计算平台编程，RoboMasterOS就是针对高性能计算平台编程的。

**高性能计算平台编程**：

- 区别于嵌入式控制编程，高性能计算平台编程一般使用性能相对较好的单板计算机或mini PC作为开发平台， 例如manifold2， tx2， NUC， hikey970， 树莓派等。
- 因为“机器视觉”，“自主导航”等算法需要较高的计算性能支持，嵌入式处理器无法处理。一般来说，嵌入式控制编程比较底层，直接涉及到机器人执行机构和大部分传感器。
- 简单点说，嵌入式控制编程是底层软件开发，高性能计算平台编程是上层软件开发，在机器人开发中，两者各有分工，并通过[基本通信](Tutorials/beginner_robot_base.md) (robot_base模块)联系在一起。

#### RoboMasterOS

* 本质上是一个ROS metapackage.

* RoboMasterOS提供两种东西：架构，软件功能模块
  * 架构：给出了模块的设计架构，模块的组织方式，以及代码编程规范，是一个统一的软件架构。
  * 软件功能模块：给出了部分功能模块，如相机模块，弹道计算模块，自瞄模块等。


特点：

- ROS兼容（基于ROS开发）：支持与ROS其他功能包，如SLAM相关功能包一起使用。
- 统一架构
- 可视化开发
- DIY模块化支持：乐高式组件开发，支持DIY开发。
- 机器人仿真开发：提供RoboMaster相关的gazebo仿真工具包（开发中）
- 文档支持：快速上手。

>  关于特点的更多内容见[Features](Get_Started/features)章节

### 适用场景

RoboMasterOS适用以下场景的高性能计算平台软件开发：

* RoboMaster对抗赛
* RoboMaster ICRA AI挑战赛
* RoboMaster大学生 夏令营
* RoboMaster中学生 夏/冬令营

RoboMasterOS提供了许多开发模块，以及开发工具，为搭载高性能计算平台的RoboMaster机器人的 **软件快速开发** 提供支持。

以上，是RoboMasterOS的主要开发场景，也是RoboMasterOS后续开发和维护的方向。

> RoboMasterOS适用场景补充
>
> * 当然，RMOS不局限于RoboMaster，使用RMOS开发其他机器人算法，或者开发一些涉及硬件控制与高性能算法的小制作(如人脸跟随云台)也是一个不错的选择。

### 目标

RoboMasterOS只有一个目的，**加速开发**。

* 降低协作成本：统一架构
  * RMOS提供了一个统一的架构，以及开发规范，避免了多人协作带来的移植难度。
* 降低开发成本：复用模块
  * RMOS具备丰富的**基础模块**，以及 **调试模块** 。ROS本身具有丰富的基础模块和强大的调试模块，RMOS根据RoboMaster自身特点，也针对性的设计了部分基础模块和调试模块。
  * RMOS兼容ROS，所以可以充分利用其他基于ROS的项目加速开发，如SLAM相关功能包。
* 快速上手，适合新手：丰富的文档
  * RMOS具备详细的文档支持，包含RoboMaster机器人开发流程样例，以及一些开发技巧，加速开发，对于新手来说，非常适合。

### 设计参考

* 基于ROS的设计方式
* 参考了[RoboRTS][4]的设计方式

> RoboRTS项目
>
> * RoboRTS项目是RoboMaster官方为RoboMaster人工智能挑战提供的开发样例。其中具有很多不错的设计思路，RMOS很多设计思路就是参考RoboRTS，不过由于RoboRTS针对于全自动机器人，其功能针对于RoboMaster人工智能挑战赛，这是它的局限性。RMOS目的是提供一个更为广泛的功能软件栈，这是不满足于RoboRTS提供的功能。

<br>



[2]: https://www.robomaster.com
[4]: https://github.com/RoboMaster/RoboRTS