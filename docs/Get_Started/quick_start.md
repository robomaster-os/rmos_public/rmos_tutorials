# 快速开始

### 1.准备工作

#### ROS与编程基础

- c++编程基础。
- python编程基础
- ros相关知识。

> 由于RMOS是基于ros的，所以需要了解相关ros基础知识，参考文档[Resource/ROS学习资源](Resource/ros.md) 即可。

#### RMOS的基本了解

希望你在使用前，先对RMOS有个基本的了解，包括它是什么（RMOS项目结构），以及能做啥（RMOS相关功能）等等。建议先阅读完下列两节：

* [Introduction](Get_Started/introduction.md)
* [Architecture](Get_Started/architecture.md)

#### RMOS环境配置

* 参考[Installation](Get_Started/installation.md)

### 2.学习路线

基础篇：RMOS的基本使用

* 基础篇基本不需要额外的代码编写，熟悉RMOS中基本模块的使用，对RMOS有关基本了解。
* 将会介绍多个有趣的功能模块，如自瞄和能量机关(2019版本)，无需编写代码，探索自瞄与能量机关算法的乐趣。

* 以单个模块的测试使用入门，降低入门难度，在最后，将会使用多个模块构建一个完整机器人项目，以步兵工程为例，学习RMOS的项目架构。

进阶篇：学习如何构建自定义RMOS模块

* 将会介绍如何开发自定义RMOS模块，主要涉及RMOS模块设计模式以及相关规范。

高级篇：扩展内容

* 无

> 学习顺序建议:
>
> * 先对ROS有个基本了解，即使用rmos所需要的最低知识要求，可参考[Resources/ROS学习资源](Resources/ros.md)。
> * 然后按照顺序把 `基础篇`和`进阶篇`学习完，就能对rmos有一个基本了解，然后进行自己的demo开发了。
> * `高级篇`可选择性学习

RM开发指南

* 在学习完tutorial内容后，可以进行自己的开发，关于RM开发相关技巧以及指南，可以参考Developers Guide部分内容

### 3.相关开发参考资料

**机器人(RM)相关优秀文章**

* [浅谈机器人比赛中的系统工程和组织管理](https://zhuanlan.zhihu.com/p/33474355) :知乎@YY硕
* [机器人工程师学习计划](https://zhuanlan.zhihu.com/p/22266788):知乎@YY硕 （深度好文，推荐）
* [从RoboMaster AI挑战赛到移动机器人系统的入坑指南](https://zhuanlan.zhihu.com/p/44117460)：知乎＠李成炎

**roborts项目**

* ICRA人工智能挑战赛官方开源项目，非常不错的项目，本项目很多地方就是参考这个项目的．
* 项目github地址：[RoboRTS](https://github.com/RoboMaster/RoboRTS)

**相关书籍**

* Probabilistic Robotics

