# RoboMasterOS项目结构

###  1.项目架构

与RoboMasterOS相关的有4个项目，

* **[RoboMasterOS项目][1]** ：公共软件栈，应包含强大的基础功能技术栈和调试功能技术栈。
* **[RMOS_contrib项目][2]** ：功能软件栈，提供各种具体算法的功能包，原则上不相互依赖，依赖于RoboMasterOS
* **[RMOS_example项目][3]** ：具体开发工程实例，依赖于RoboMasterOS，选择性依赖RMOS_contrib，以及其他包。
* **[RMOS_simulator项目][4]** ：Gazebo仿真工具包，为RoboMaster提供Gazebo仿真机器人模型以及相关工具，为RMOS开发提供仿真平台

### 2.RoboMasterOS模块概览

**[RoboMasterOS][1]:** （6 packages）

|             模块             |                           功能概况                           |
| :--------------------------: | :----------------------------------------------------------: |
|         `robot_base`         | 基本通信模块，实现与底层通信，可以控制底层，并从底层获得相关数据。 |
|         `robot_cam`          | 相机模块，实现usb相机驱动，以及图片视频模拟成相机（方便测试）。 |
|         `robot_msgs`         |                   基本自定义ROS message。                    |
|         `robot_tool`         |            基本工具包，包括调试，图像处理等工具。            |
| `projectile_trajectory_tool` |   子弹弹道修正工具，可以修正子弹飞行过程中重力因素的影响。   |
|     `task_image_related`     |         基本图像相关任务工具，加速图像任务开发速度。         |

**[RMOS_contrib][2]:** （2 packages）

|         模块          |                          功能概况                          |
| :-------------------: | :--------------------------------------------------------: |
|    `task_auto_aim`    |          自瞄任务，实现了RoboMaster比赛自瞄功能。          |
| `task_power_rune2019` | 能量机关功能任务，实现了RoboMaster2019赛级的能量机关功能。 |

**[RMOS_example][3]:**（1 packages）

|           模块           |           功能概况           |
| :----------------------: | :--------------------------: |
| `standard_robot_example` | RoboMaster步兵机器人项目例子 |

**[RMOS_simulator][4]**（4 packages）

|              模块               |                功能概况                |
| :-----------------------------: | :------------------------------------: |
|         `rmos_sim_msgs`         |        仿真相关自定义消息类型。        |
|      `rmos_gazebo_plugins`      |    针对RoboMaster相关的Gazebo插件。    |
|       `rmos_standardbot`        | 简易RoboMaster步兵机器人Gazebo仿真模型 |
| `rm2019_icra_simulator(未完成)` |  RoboMaster2019 ICRA AI挑战赛仿真平台  |



[1]: https://gitlab.com/robomaster-os/rmos_public/robomaster_os	"robomaster_os"
[2]: https://gitlab.com/robomaster-os/rmos_public/rmos_contrib	"rmos_contrib"
[3]: https://gitlab.com/robomaster-os/rmos_public/rmos_example	"rmos_example"
[4]: https://gitlab.com/robomaster-os/rmos_public/rmos_simulator	"rmos_simulator"

 