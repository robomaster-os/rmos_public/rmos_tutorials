# 4.自瞄功能开发案例

### 任务简述

视觉任务是robomaster算法开发（robomaster算法组或视觉组）的主要任务，而自动瞄准又是视觉任务的核心任务，在robomaster比赛中占有重要地位。

RMOS_contrib项目中，提供了一个简单的自瞄开发样例，本次教程将简要的介绍该自瞄样例模块使用，以及解析如何基于RMOS开发一个视觉任务。

本次测试不涉及硬件，使用图片来模拟摄像头获取的图像。

### 环境准备

模块依赖:

- robomaster_os（metapackage）

  - robot_cam
  - robot_msgs
  - robot_tool
  - task_image_related
  - projectile_trajectory_tool
- rmos_contrib（metapackage）
  - task_auto_aim
- rmos_simulator

### 快速运行

#### 自瞄测试（图片模拟相机）

launch启动

```bash
roslaunch task_auto_aim task_with_image_cam.launch
```

查看相机图形

```bash
rqt_image_view
```

![](imgs/beginner_auto_aim_1.png)

使用client_test.py启动自瞄任务

```bash
rosrun task_auto_aim client_test.py 
```

- 根据提示启动（按`q`）

debug信息（图像处理中间过程信息）

![](imgs/beginner_auto_aim_2.png)

若想取消图像debug信息，可以修改配置文件res/task_config.yaml

```yaml
#debug flag
is_debug_show_img : 0
```

#### 自瞄测试（Gazebo仿真机器人）

`注意`：该部分依赖于rmos_simulator。

laucnh启动

```bash
roslaunch task_auto_aim task_with_gazebo.launch
```

使用client_test.py启动自瞄任务

```bash
rosrun task_auto_aim client_test.py 
```

- 根据提示启动（按`q`）

识别结果

![](imgs/beginner_auto_aim_3.png)

gazebo仿真射击情况

![](imgs/beginner_auto_aim_4.png)

可以控制红色机器人移动，观察跟踪效果

```bash
rosrun rmos_standardbot robot2_contro.py 
```

- 如果控制蓝色机器人使用robot1_contro.py即可

* 更多使用参考[task_auto_aim repo][1]。

### 自瞄算法说明:

自瞄任务流程：

视觉任务的一般流程进行设计，本次测试不涉及硬件。同时，也将利用RoboMasterOS工具，减小开发量，专注于自瞄图像处理算法的开发。在本次自瞄任务开发中，对应的流程具体如下:

* 参考[3.图像处理任务](Tutorials/beginner_image_task.md)

-  我们只需要专注于图像处理部分即可。

算法处理过程：

* step1.装甲板识别：识别装甲板，输入图片，输出装甲板列表

* step2.位置解算：对于识别到装甲板，计算其3d位置
* step3.目标选择：若出现多个装甲板，需要选择最优打击目标

* step4.重力补偿：子弹的飞行路径不是直线，需要考虑重力等因素的补偿，计算实际射击，云台所需要的偏转角度。

* step5.发送指令：将计算得到的云台偏转角度发送给机器人，控制机器人瞄准。

具体实现

- 更多说明参考[task_auto_aim repo][1]。



[1]: https://gitlab.com/robomaster-os/rmos_public/rmos_contrib/tree/master/task_auto_aim

 