# 基础篇：使用RMOS

基础篇：熟悉RMOS的使用，对RMOS中的模块与工程使用有基本的了解

依赖：

* robomaster_os
* rmos_contrib 
* rmos_simulator
* rmos_example

> 请提前配置好RMOS环境:[installation](Get_started/installation.md)
>
> 此外，还需要下载RMOS依赖包
>
> ```bash
> git clone https://gitlab.com/robomaster-os/rmos_public/rmos_contrib.git
> git clone https://gitlab.com/robomaster-os/rmos_public/rmos_simulator.git
> git clone https://gitlab.com/robomaster-os/rmos_public/rmos_example.git
> ```
> 编译：进入catkin_ws目录下
>
> ```bash
> catkin_make
> ```
>

内容：

- 基础篇基本不需要额外的代码编写，熟悉RMOS中基本模块的使用，对RMOS有关基本了解。
- 将会介绍多个有趣的功能模块，如自瞄和能量机关(2019版本)，无需编写代码，探索自瞄与能量机关算法的乐趣。
- 以单个模块的测试使用入门，降低入门难度，在最后，将会使用多个模块构建一个完整机器人项目，以步兵工程为例，学习RMOS的项目架构
