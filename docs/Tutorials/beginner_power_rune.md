# 5.能量机关可视化开发案例

### 任务简述

这里以能量机关开发作为⼀个例子，学习如何基于RMOS（ROS）进行可视化开发，不需要任何编码，利用rmos提供现成的demo，熟悉开发流程。其中，主要依赖robot_cam和task_power_rune2019两个模块。
参考文章：

* [关于RoboMaster2019能量机关任务](https://www.jianshu.com/p/83b509953198)

### 环境准备

模块直接依赖：

- robomaster_os（metapackage）
  - robot_cam
- rmos_contrib（metapackage）
  - task_power_rune2019
- rmos_simlator（metapackage）
  - rmos_standardbot

### 快速运行

#### 能量机关测试（视频模拟相机）

修改launch文件:launch/task_with_video_cam.launch

```xml
<?xml version="1.0"?>
<launch>
  <!--cam node-->
  <include file="$(find robot_cam)/launch/sim_cam_video.launch">
    <arg name="video_path" value="/home/gezp/rune_res/whole_motion.avi" />
  </include>
  <!--task node-->
  <node name="task_power_rune2019" pkg="task_power_rune2019" type="task_power_rune_node" output="screen" 
        respawn="false" args="$(find task_power_rune2019)/res/task_config.yaml"/>
</launch>
```

- 更改视频路径，将video_path对应的value改成自己能量机关视频路径
- 能量机关测试视频官方下载：[RoboMaster论坛链接](https://bbs.robomaster.com/thread-7914-1-1.html)：[百度云盘](https://pan.baidu.com/s/1OoIpbzZ2U8Qimadn3zyb6g)

> tip: 为了方便测试，这里使用视频模拟相机，根据需要，你也可以使用usb相机，只需连接usb相机后，启动usb相机节点即可，同样，还可以使用自定义的工业相机等等，详细使用，参考robot_cam文档。

launch启动

```bash
roslaunch task_power_rune2019 task_with_video_cam.launch
```

查看相机图形

```bash
rqt_image_view
```

![](imgs/beginner_power_rune_1.png)

使用client_test.py启动自瞄任务

```bash
rosrun task_power_rune2019 client_test.py 
```

- 根据提示启动（按`s`）

debug信息（图像处理中间过程信息）

![](imgs/beginner_power_rune_2.png)

若想取消图像debug信息，可以修改配置文件res/task_config.yaml

```yaml
#debug flag
is_debug_show_img : 0
```

#### Rviz可视化打击点

启动能量机关任务

- 可按照`能量机关测试（视频模拟相机）`操作。

使用launch启动rviz

```bash
roslaunch rviz_visulazation.launch
```

- 最开始什么都没有，因为还没有产生打击点。

使用client_test.py启动自瞄任务，并不断产生打击点

```bash
rosrun task_power_rune2019 client_test.py 
```

- 根据提示启动（按`s`）
- 不断产生打击点（按`r`），并可以在rviz上观察到。

![](imgs/beginner_power_rune_3.png)

- 可以用于评估识别误差

更多使用参考 [task_power_rune2019 repo][1]





[1]: https://gitlab.com/robomaster-os/rmos_public/rmos_contrib/tree/master/task_power_rune2019