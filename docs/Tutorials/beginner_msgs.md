# 2.常用通信消息类型

### ROS通信

RMOS是基于ROS的，ROS的通信机制是重要内容之一。RMOS的模块化设计，每一个功能模块都是一个单独的节点（独立进程），所有功能模块通过通信机制连接起来，构成机器人的整体功能。

> ROS支持三种通信方式：topic,service,action
>
> * 其中topic和service是比较常用的方式
> * 一般数据传输采用topic方式，支持多对多，通过publish更新数据，subscribe获取数据。如云台角度数据等。
> * 功能服务采用service方式，如自瞄功能开启与关闭等。

涉及模块：

* robot_msgs：RoboMasterOS 中的一个基础功能包，提供了相关基础消息定义

### 常用topic消息类型

GimbalInfo.msg：云台信息

```bash
#gimbal module data,include pitch and yaw.
########### id of publisher
uint8 id
#control type
#  0x00 relative
#  0x01 absolute
#  0x02 local scan
#  0x03 global scan
#  other custom usage
uint8 type
########
float32 yaw
float32 pitch
########
uint8 yaw_angular_vel
uint8 pitch_angular_vel
########
float32 yaw_scan_min
float32 yaw_scan_max
```

ChassisInfo.msg：底盘信息

* 参考[robot_msgs repo][1]

RailChassisInfo.msg：轨道底盘信息（针对哨兵）

* 参考[robot_msgs repo][1]

LaunchInfo.msg：射击信息

* 参考[robot_msgs repo][1]

### 常用service消息类型

SetMode.srv：模式设置

```bash
#request constants
int8 mode
int32 data
---
#response constants
bool success
```



[1]: https://gitlab.com/robomaster-os/rmos_public/robomaster_os/tree/master/robot_msgs

 