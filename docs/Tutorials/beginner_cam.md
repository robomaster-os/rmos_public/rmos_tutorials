# 1.相机模块的使用

### RoboMaster中的视觉

在RoboMaster比赛中，相机就是机器人的眼睛，是机器人获取外界信息的重要途经，通过计算机视觉处理，从而可以做出一些复杂的行为，如备受强队青睐的杀手锏功能能量机关，或者开挂般的自瞄功能。除此之外，比赛中越来越重视视觉的应用，如RoboMaster2020赛季，雷达站等规则，使得视觉应用具有非常大潜力。

在robomaster_os中，提供了robot_cam模块，定义了一套通用相机API，实现了普通usb相机，图片模拟相机，视频模拟相机三种相机采集功能，并支持自定义工业相机扩展。

> 相机是一种非常重要的光学传感器，其种类繁多，所以相机选型也是一种学问，从百元价位的普通OV系列相机到上万的工业相机，以及不同焦距镜头的选型，这都需要针对比赛进行专门选择，这里不再详述，可参考资料[RM相机选型指南](Developers_Guide/camera_selection.md)

本文涉及模块：

- robot_cam

### robot_cam模块

#### usb相机：

修改配置文件：res/usb_cam_config.yaml

```yaml
#video_path，可自行修改
usb_cam_path : "/dev/video0"
```

运行说明：

```bash
rosrun robot_cam usb_cam_node [optional config_path] #默认使用usb_cam_config.yaml
```

#### 图片模拟相机：

运行说明：

```yaml
#需要两个参数
rosrun robot_cam sim_cam_image_node [config_path] [img_path]
```

运行：(采用launch方式)

```bash
roslaunch robot_cam sim_cam_image.launch
```

#### 视频模拟相机：

运行说明：

```yaml
#需要两个参数
rosrun robot_cam sim_cam_video_node [config_path] [video_path]
```

* 无需设置cam_fps，相机的FPS值将自动设为视频的帧率，无法修改。

运行：(采用launch方式)

```bash
roslaunch robot_cam sim_cam_video.launch
```

* 无法运行，需要设置launch文件中的视频路径参数才能运行

更多内容参考[robot_cam repo][1]

### 自定义robot_cam开发

* 更多内容参考[robot_cam repo][1]



[1]: https://gitlab.com/robomaster-os/rmos_public/robomaster_os/tree/master/robot_cam

 