# 2.步兵机器人工程项目

### 机器人工程项目

RoboMaster机器人是一个多学科交叉的大项目，其功能复杂程度较高，所以在软件开发过程中，RMOS采用了模块化，组件化的设计思路，最终组装成一个整体。这篇文章以一个简单的步兵机器人工程项目简单的展示了模块设计与模块组装。

### 环境依赖

- robomaster_os（metapackage）
  - robot_cam
  - robot_msgs
  - robot_tool
- rmos_contrib（metapackage）
  - task_auto_aim
  - task_power_rune2019
- rmos_example（metapackage）
  - standard_robot_example

将相应代码使用git下载到catkin_ws/src目录下

```bash
git clone
```

编译：进入catkin_ws目录下

```bash
catkin_make
```

### standard_robot_example项目

launch启动运行

```bash
roslaunch standard_robot_example standard_robot.launch
```

启动了4个节点

* standard_robot_base
* robot_cam
* task_auto_aim
* task_power_rune2019

> 如果没有实体机器人，可以运行测试节点来进行功能测试，如：
>
> ```bash
> # 启动或暂停自瞄功能
> rosrun task_auto_aim client_test.py
> ```

ros系统节点示意图

* 略