# 2.RMOS模块项目规范

基于RMOS开发项目，本质上构建一个ROS package，所以项目工程结构以ROS项目工程为基础。

* 工程项目目录结构完全与ROS项目一致。
* CMakeLists文件编写与ROS项目一致，并提供模板，方便自行编写。

### 项目工程目录结构

以robot_exmaple项目为例

```bash
├── include
│   └── robot_exmaple #主要代码include目录
├── src     #主要代码src目录
├── nodes   #ros node目录，程序main函数入口
├── launch  #launch启动文件目录
├── scripts #python等脚步存放目录
├── doc　　　#文档目录
├── CMakeLists.txt
├── package.xml
└── README.md
```

### CMakeLists规范

以下为robot_test的CMakeLists.txt的文件例子

```bash
cmake_minimum_required(VERSION 3.1)
project(robot_test)

set(CMAKE_CXX_STANDARD 11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  robot_msgs
)

find_package(OpenCV 3 REQUIRED)

catkin_package(
   INCLUDE_DIRS include
   LIBRARIES ${PROJECT_NAME}
   CATKIN_DEPENDS roscpp robot_msgs
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

#robot_test lib
AUX_SOURCE_DIRECTORY(${PROJECT_SOURCE_DIR}/src DIR_SRCS)
add_library(${PROJECT_NAME} SHARED ${DIR_SRCS})
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)
add_dependencies(${PROJECT_NAMEW} robot_msgs_generate_messages)
## robot_test_node
add_executable(robot_test_node nodes/robot_test_node.cpp)

target_link_libraries(robot_test_node
   ${catkin_LIBRARIES}
   ${OpenCV_LIBRARIES}
   ${PROJECT_NAME}
)
```

上述CMakeLists编写，对于一般项目而言，满足需求。以下为详细解释：

```bash
#申明CMake最小版本为3.5
cmake_minimum_required(VERSION 3.5)
```

```bash
#项目名字为robot_test
project(robot_test)
```

```bash
#设置CMake一些属性，
## 采用C++11标准
set(CMAKE_CXX_STANDARD 11)
```

```bash
#依赖库查找
##ROS内部依赖，依赖ROS的一些package，如roscpp，rospy，std_msgs等
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  robot_msgs
)
##其他依赖，这里依赖OpenCV
find_package(OpenCV 3 REQUIRED)
```

```bash
#ROS package配置
catkin_package(
   INCLUDE_DIRS include
   LIBRARIES ${PROJECT_NAME}　
   CATKIN_DEPENDS roscpp robot_msgs　#该模块依赖的ROS库
)
######额外说明#########
#如果该项目，即该package，不会被其他package依赖，即不会被其他模块链接，只需以下形式即可。
catkin_package()
```

```bash
#头文件包含目录，即该项目中，include寻找头文件的目录。
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)
```

```bash
#生成robot_test库
AUX_SOURCE_DIRECTORY(${PROJECT_SOURCE_DIR}/src DIR_SRCS)
add_library(${PROJECT_NAME} SHARED ${DIR_SRCS})
## 链接
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)
## 依赖robot_msgs生成的文件（即编译该模块前，需要先编译完robot_msgs）
add_dependencies(${PROJECT_NAME} robot_msgs_generate_messages)
```

```bash
#生成robot_test_node可执行文件
add_executable(robot_test_node nodes/robot_test_node.cpp)
##链接
target_link_libraries(robot_test_node
   ${catkin_LIBRARIES}
   ${OpenCV_LIBRARIES}
   ${PROJECT_NAME}
)
```



### README规范

至少包含项目说明，使用说明，版权信息三部分内容