# 4.rmos模块自定义开发案例

基于robot_base的standard_robot_base模块

* 复用robot_base中的serial_port_dev数据传输类和fixed_packet数据封装类。
* 只需要完成协议部分即可，大大降低了工作量。

task_image_related模块

* 继承task_image_related中的基类，无需关心ROS接收图像层面，只需要实现`taskImageProcess`函数即可实现图像处理任务。
* 被task_auto_aim和task_power_rune2019应用。

robot_tool模块

* 提供了一些基本类，被其它模块广泛应用，如单目测量封装函数（PnP）等等
* 被task_auto_aim和task_power_rune2019等模块应用。

projectile_trajectory_tool模块

* 提供了重力补偿工具类，被task_auto_aim和task_power_rune2019应用。