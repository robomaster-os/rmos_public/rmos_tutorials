

# Tutorials Web for RoboMasterOS  

[![pipeline status](https://gitlab.com/robomaster-os/rmos_tutorials/badges/master/pipeline.svg)](https://gitlab.com/robomaster-os/rmos_tutorials/commits/master)

## RoboMasterOS

* RoboMasterOS是一个RoboMaster相关的**开源软件栈（RoboMaster Open Stack,简称RMOS）** ，也是一个通用统一的robomaster机器人视觉算法软件架构，基本目标是快速进行算法验证，功能验证，想法验证。

特点：

* ROS兼容（基于ROS开发）
* 统一架构
* 可视化开发
* DIY模块化支持
* 机器人仿真开发
* 文档支持

