# ROS参考学习资料

ROS的学习，建议参考官网wiki学习，只需要学习beginner部分即可

* 官网[wiki](http://wiki.ros.org/)

RMOS中用到ROS具体地方

- 项目工程与文件结构（package）
- 编译系统（catkin_make：基于cmake）
- 通信机制：
  - 话题topic
  - 服务service
- 简单命令及调试工具（rosrun，roslaunch，rostopic，rqt_image_view等）